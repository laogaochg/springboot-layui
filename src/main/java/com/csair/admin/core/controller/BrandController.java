package com.csair.admin.core.controller;

import com.csair.admin.core.po.Brand;
import com.csair.admin.core.po.core.PageResult;
import com.csair.admin.core.po.core.ResponseMessage;
import com.csair.admin.core.po.core.ReturnMessage;
import com.csair.admin.core.po.core.User;
import com.csair.admin.core.po.core.query.BrandQueryObject;
import com.csair.admin.core.po.core.resp.DatagridForLayUI;
import com.csair.admin.core.service.BrandService;
import com.csair.admin.core.service.MenuService;
import com.csair.admin.util.FileUploadUtils;
import com.csair.admin.util.ParamConstants;
import com.csair.admin.util.ServletUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * laogaochg
 * 2017/7/25.
 */
@Controller
@RequestMapping("brand")
public class BrandController {
    private static Logger logger = LoggerFactory.getLogger(BrandController.class);

    @Resource
    private BrandService brandService;

    @RequestMapping("/list")
    @ResponseBody
    public DatagridForLayUI<Brand> brandList(BrandQueryObject qo) {
        PageResult<Brand> pageResult = brandService.pageQuery(qo);
        return new DatagridForLayUI<>(pageResult);
    }
    @RequestMapping("/BrandDetails")
    @ResponseBody
    public ResponseMessage brandDetails(Brand brand) {
        Brand b =  brandService.queryById(brand.getBrandId());
        b.setBrandDesc(HtmlUtils.htmlUnescape(b.getBrandDesc()));
        return new ResponseMessage(b);
    }



    @RequestMapping("delete")
    @ResponseBody
    public ResponseMessage batchDeleteBrand(Long[] ids) {
        if (ids == null || ids.length == 0) {
            ResponseMessage re = new ResponseMessage();
            re.setCode(ParamConstants.ERROR_PARAM);
            re.setMsg("id不能为空。");
            return re;
        }
        return brandService.batchDeleteBrand(ids, ServletUtils.getUser());
    }

    @RequestMapping("/toEditBrand")
    public String editCertificate(Long id, Model model) {
        if (id != null) {
            model.addAttribute("brand", brandService.queryById(id));
        }
        return "goodManage/brand/editBrand";
    }

    @RequestMapping("/editBrand")
    @ResponseBody
    public ResponseMessage editBrand(Brand brand, Model model) {
        ResponseMessage re = new ResponseMessage();
        model.addAttribute("msg", re);
        //前台数据验证
        if (!StringUtils.hasText(brand.getBrandName())) {
            re.setCode(ParamConstants.ERROR_PARAM );
            re.setMsg("品牌名字不能为空。");
            return re;
        }
        if (!StringUtils.hasText(brand.getBrandLogo())) {
            re.setCode(ParamConstants.ERROR_PARAM );
            re.setMsg("品牌logo不能少。");
            return re;
        }
        if (brand.getBrandOrder() == null) brand.setBrandOrder(1);
        //保存压缩图片
        String reduceImg = FileUploadUtils.reduceImg(brand.getBrandLogo());
        brand.setBrandLogoThumb(reduceImg);
        User user = ServletUtils.getUser();
        brandService.editBrand(brand, user);
        if (brand.getBrandId() == null) {
            re.setMsg("添加成功。");
        } else {
            re.setMsg("修改成功。");
        }
        return re;
    }

}
