package com.csair.admin.core.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/3/29 21:31
 */
@Data
public class GoodsSkuDto {
    private Long id;
    //价格
    private Long sellPrice;

}
