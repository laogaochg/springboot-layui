package com.csair.admin.core.po.core.query;

/**
 * laogaochg
 * 2017/7/24.
 */
public class MenuQueryObject extends QueryObject {
    private String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
