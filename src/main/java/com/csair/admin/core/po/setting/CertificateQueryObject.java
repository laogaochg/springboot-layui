package com.csair.admin.core.po.setting;

import com.csair.admin.core.po.core.query.QueryObject;

/**
 * laogaochg
 * 2017/7/24.
 */
public class CertificateQueryObject extends QueryObject {
    private String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
