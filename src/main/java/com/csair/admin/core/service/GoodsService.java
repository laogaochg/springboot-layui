package com.csair.admin.core.service;

import com.csair.admin.core.dto.GoodsDto;
import com.csair.admin.core.vo.GoodsVo;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/3/29 21:38
 */
public interface GoodsService {
    int insertGoods(GoodsVo goods);
}
