<meta charset="utf-8">
<script type="text/javascript">
    // 配置参数
    var contextPath = '${context.contextPath}';
</script>
<#--jquery-->
<script src="${context.contextPath}/js/jquery/jquery.js"></script>
<#--bootstrap-->
<link href="${context.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="${context.contextPath}/js/bootstrap/bootstrap.min.js"></script>
<#--bookstrap 分页插件-->
<script src="${context.contextPath}/js/jquery/jquery.twbsPagination.min.js"></script>
<#--layui-->
<link rel="stylesheet" href="${context.contextPath}/js/layui/css/layui.css">
<script src="${context.contextPath}/js/layui/layui.js"></script>
<#--项目自定义-->
<link rel="stylesheet" href="${context.contextPath}/css/index/mybase.css">
<link rel="stylesheet" href="${context.contextPath}/css/index/style.css">
<#--左边菜单选中事件-->
<script src="${context.contextPath}/js/common/main.js"></script>
