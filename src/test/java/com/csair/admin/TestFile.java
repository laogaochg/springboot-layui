package com.csair.admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.csair.admin.util.XlsFileUtil;
import org.junit.Test;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/7/9 11:58
 */
public class TestFile {
    /**
     * 得到时间段的string，每五分钟一次
     *
     * @param form 时间的格式
     */
    public static Set<Long> getShouldSet(long begin, long end, SimpleDateFormat form) {
        Set<Long> shouldSet = new TreeSet<>();
        while (begin <= end) {
            Date date = new Date(begin);
            String format = form.format(date);
            shouldSet.add(Long.valueOf(format));
            //每次加五分钟
            begin += 5 * 60 * 1000;
        }
        return shouldSet;
    }

    @Test
    public void testooaa() throws Exception {
        Set<String> strList = new HashSet<>();
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < 1000000; i++) {
            strList.add(String.valueOf(i));
            set.add(i);
        }
        long l = System.currentTimeMillis();
        Iterator<String> strOr = strList.iterator();
        while (strOr.hasNext()) {
            strOr.next();
            strOr.remove();
        }
        long d = System.currentTimeMillis();
        System.out.println(d - l);
        Iterator<Integer> setOr = set.iterator();
        while (setOr.hasNext()) {
            setOr.next();
            setOr.remove();
        }
        long e = System.currentTimeMillis();
        System.out.println(e - d);
    }

    @Test
    public void testooa() throws Exception {
        String name = "hz_message-201812-localhosth";
        File f = new File("D:\\data\\采集的数据/" + name + ".dat");
        BufferedReader reader = new BufferedReader(new FileReader(f));
        TreeMap<Long, List<JSONObject>> map = new TreeMap<>();
        while (reader.ready()) {
            String s = reader.readLine();
            JSONObject jo = JSON.parseObject(s);
            Long dt = Long.valueOf(jo.get("dt") + "");
            List<JSONObject> list = map.get(dt);
            if (list == null) {
                list = new ArrayList();
                map.put(dt, list);
            }
            list.add(jo);
        }
        List<List<String>> data = new ArrayList<>();

        long begin = new SimpleDateFormat("yyyyMMdd-HH:mm").parse("20181201-00:00").getTime();
        long end = new SimpleDateFormat("yyyyMMdd-HH:mm").parse("20181231-23:59").getTime();
        Set<Long> shouldSet = getShouldSet(begin, end, new SimpleDateFormat("yyyyMMddHHmmss"));
        for (Long aLong : shouldSet) {

//        }
//        for (Map.Entry<Long, List<JSONObject>> entry : map.entrySet()) {
            List<String> list = new ArrayList<>();
            data.add(list);
            Long key = aLong;
            list.add(key + "");
            List<JSONObject> value = map.get(key);
            BigDecimal countIn = BigDecimal.ZERO;
            BigDecimal countOut = BigDecimal.ZERO;
            boolean hasLess = false;
            if (value != null) {
                for (JSONObject jsonObject : value) {
                    BigDecimal ifInOctetsBefore = new BigDecimal(jsonObject.get("ifInOctetsBefore") + "");
                    BigDecimal ifOutOctetsBefore = new BigDecimal(jsonObject.get("ifOutOctetsBefore") + "");
                    countIn = countIn.add(ifInOctetsBefore);
                    countOut = countOut.add(ifOutOctetsBefore);
                    hasLess = countIn.compareTo(BigDecimal.ZERO)<0||countOut.compareTo(BigDecimal.ZERO)<0;
                }
                int i = 1024 * 1024;
                countOut = countOut.divide(new BigDecimal(i)).setScale(3, BigDecimal.ROUND_UP);
                countIn = countIn.divide(new BigDecimal(i)).setScale(3, BigDecimal.ROUND_UP);
                list.add(countOut + "");
                list.add(countIn + "");
                list.add((countOut.add(countIn)) + "");
            } else {
                list.add(countOut + "");
                list.add(countIn + "");
                list.add((countOut.add(countIn)) + "");
                list.add("缺少数据");
            }
            if(hasLess){
                list.add("有负值");
            }
        }
        XlsFileUtil.createExcel(new File("d:/" + name + ".xls"), data);
    }

    @Test
    public void testoo() throws Exception {
        File f = new File("D:\\a.txt");
        BufferedReader reader = new BufferedReader(new FileReader(f));
        TreeMap<Long, List<JSONObject>> map = new TreeMap<>();
        while (reader.ready()) {
            String s = reader.readLine();
            String[] split = s.split("\\|");
            JSONObject jo = JSON.parseObject(split[0]);
            String dt = String.valueOf(jo.get("dt"));
            BigDecimal in = new BigDecimal(jo.get("ifInOctetsBefore") + "");
            BigDecimal out = new BigDecimal(jo.get("ifOutOctetsBefore") + "");
            BigDecimal inOrcale = new BigDecimal(split[1].trim());
            BigDecimal outOrcale = new BigDecimal(split[2].trim());
            BigDecimal d = new BigDecimal(1);
            if (!inOrcale.equals(BigDecimal.ZERO)) {
                out = out.divide(outOrcale, BigDecimal.ROUND_HALF_UP).divide(d, BigDecimal.ROUND_HALF_UP);
                in = in.divide(inOrcale, BigDecimal.ROUND_HALF_UP).divide(d, BigDecimal.ROUND_HALF_UP);
                System.out.println(String.format("%s | %s | %s", dt, out.toString(), in.toString()));
            } else {
                out = out.divide(d, BigDecimal.ROUND_HALF_UP);
                in = in.divide(d, BigDecimal.ROUND_HALF_UP);
                System.out.println(String.format("%s | %s | %s | oracle值为0", dt, out.toString(), in.toString()));
            }
        }
    }

    @Test
    public void testaa() throws Exception {
        List<List<String>> lists = XlsFileUtil.parseXlsFile(new FileInputStream(new File("d:/a.xls")));
        Set<String> other = new HashSet<>();
        for (List<String> list : lists) {
            other.add(list.get(4));
        }
        List<String> strings = MyFileUtils.parseFile("d:/a.rsp");
        Set<String> my = new HashSet<>();
        for (String s : strings) {
            my.add(s.split("\\|")[6]);
        }
        for (String s : other) {
            if(!my.contains(s)) System.out.println(s);

        }
        /*List<String> strings = MyFileUtils.parseFile("d:/a.rsp");
        Set<String> my = new HashSet<>();
        for (String s : strings) {
            my.add(s.split("\\|")[6]);
        }
        strings = MyFileUtils.parseFile("d:/a.csv");
        Set<String> other = new HashSet<>();
        for (String s : strings) {
            other.add(s.split(",")[0]);
        }
        for (String s : other) {
            if(!my.contains(s)){
                System.out.println(s);
            }
        }*/
    }
}
