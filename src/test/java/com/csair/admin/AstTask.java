package com.csair.admin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AstTask  implements Serializable {
    /**
     * 任务ID
     */
    private Long id;
    /**
     * 任务类型
     */
    private String taskApplication;
    /**
     * 任务处理类
     */
    private String taskHandler;
    /**
     * 任务参数
     */
    private String taskParams;
    /**
     * 业务ID
     */
    private Long businessId;
    /**
     * 任务创建时间
     */
    private Date createDate;
    /**
     * 任务待执行时间
     */
    private Date executeDate;
    /**
     * 重试次数，每次+1
     */
    private BigDecimal retryCount;
    /**
     * 上次执行失败的原因，便于排查
     */
    private String reason;
    /**
     * 待执行任务获取的负载均衡值
     */
    private BigDecimal balance;
    /**
     * 上次执行耗时，单位毫秒
     */
    private BigDecimal duration;
    /**
     * 上次执行结束时间
     */
    private Date endDate;
    /**
     * 上次执行失败的环节，由业务处理代码在环节处理前写入
     */
    private String step;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskApplication() {
        return taskApplication;
    }

    public void setTaskApplication(String taskApplication) {
        this.taskApplication = taskApplication == null ? null : taskApplication.trim();
    }

    public String getTaskHandler() {
        return taskHandler;
    }

    public void setTaskHandler(String taskHandler) {
        this.taskHandler = taskHandler == null ? null : taskHandler.trim();
    }

    public String getTaskParams() {
        return taskParams;
    }

    public void setTaskParams(String taskParams) {
        this.taskParams = taskParams == null ? null : taskParams.trim();
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getExecuteDate() {
        return executeDate;
    }

    public void setExecuteDate(Date executeDate) {
        this.executeDate = executeDate;
    }

    public BigDecimal getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(BigDecimal retryCount) {
        this.retryCount = retryCount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step == null ? null : step.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", taskApplication=").append(taskApplication);
        sb.append(", taskHandler=").append(taskHandler);
        sb.append(", taskParams=").append(taskParams);
        sb.append(", businessId=").append(businessId);
        sb.append(", createDate=").append(createDate);
        sb.append(", executeDate=").append(executeDate);
        sb.append(", retryCount=").append(retryCount);
        sb.append(", reason=").append(reason);
        sb.append(", balance=").append(balance);
        sb.append(", duration=").append(duration);
        sb.append(", endDate=").append(endDate);
        sb.append(", step=").append(step);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}