package com.csair.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class DateUtils {
	
	
	public static final String MONTH_FORMAT = "yyyyMM";
	public static final String DATE_FORMAT = "yyyyMMdd";
    public static final String DATETIME_FORMAT = "yyyyMMddHHmmss";
	
    /**
     * 向过去推迟的月数(自然月)
     * @param date 指定日期
     * @param format yyyyMM或者yyyyMMdd
     * @param count
     * @return
     */
	public static boolean dateFormatBef(String date,String format,int count){
		
		SimpleDateFormat f  = new SimpleDateFormat(format);
		
		//1.格式
		try {
			f.parse(date);
			
			//2.是否位于最近几个月范围内
			//当前
			String currDate = format(new Date(),format);
			//推移月份
			String befDate = getBefMonth(currDate,count);
			//yyyyMMdd时，注意加上dd(01)
			if(format.equals(DATE_FORMAT)){
				befDate = befDate + "01";
			}
			 
			Integer dateInt = Integer.parseInt(date);
			Integer currDateInt = Integer.parseInt(currDate);
			Integer befDateInt = Integer.parseInt(befDate);

			//位于指定范围外
			if(dateInt < befDateInt || dateInt > currDateInt){
				return false;
			}
			
			return true;
			
		} catch (Exception e) {
			return false;
		}


	}
	
    /**
     * 本月月第一天0点
     * @return
     */
    public static Date currMonthFirstDateTime(){
    	
    	//获取当前时间：
    	Calendar c = Calendar.getInstance();

    	//设置为1号,当前日期既为本月第一天 
    	c.set(Calendar.DAY_OF_MONTH,1);
    	c.set(Calendar.HOUR_OF_DAY,0);
    	c.set(Calendar.MINUTE,0);
    	c.set(Calendar.SECOND,0);
    	c.set(Calendar.MILLISECOND,0);
    	
    	return c.getTime();
    }

    
    /**
     * @param date      时间
     * @return
     */
    public static String format(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
        	return sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return "";
    }
    
    
    /**
     * 向过去推迟的月数
     * @param date yyyyMM或者yyyyMMdd
     * @param count 推迟的月数
     * @return
     */
	public static String getBefMonth(String date,int count){
		
	    int intMonth = Integer.valueOf(date.substring(4,6));
		int intYear = Integer.valueOf(date.substring(0,4));
		
		intMonth = intMonth-count+1;
		
	    if(intMonth<10 && intMonth>0){
	    	 return (intYear+"0"+intMonth);
	    }
	    //向过去跨年
	    if(intMonth<=0){
	    	intMonth = 12+intMonth;
	    	intYear = intYear - 1;
	    	
	    	if(intMonth<10){
	    		return (intYear+"0"+intMonth);
	    	}
	    }
	    return (intYear+""+intMonth);
	}
    
	
	/**
	 * 当前月，yyyyMM
	 * @return
	 */
	public static String currentMonth(){
		
		return format(new Date(), MONTH_FORMAT);
	}
	
	/**
	 * 当前月，MM
	 * @return MM
	 */
	public static String currentMonth1(){
		
		return format(new Date(), "MM");
	}
	
	
	/**
	 * 当前日
	 * @return yyyyMMdd
	 */
	public static String currentDay(){
		
		return format(new Date(), DATE_FORMAT);
	}
	
    /**
     * 获取两个日期之间的所有日期,包含开始与结束
     * @param startDate,yyyyMMdd
     * @param endDate,yyyyMMdd
     * @return
     */
    public static List<String> currMonthDates(String startDate,String endDate){

    	List<String> list = new ArrayList<String>();
    	
    	try {
	    	SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT);
	    	
			long start = fmt.parse(startDate).getTime();
			long end = fmt.parse(endDate).getTime();
	
			for (long i = start; i < end + (1000 * 24 * 3600); i = i + (1000 * 24 * 3600)) {
				
				Date date = new Date(i);
				String dateStr = fmt.format(date);
				
				list.add(dateStr);
				
//				System.out.println("dateStr="+dateStr);
				
			}
	
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return list;

    }
    
	/**
	 * 字符串转date
	 * @param str
	 * @param format
	 * @return
	 */
    public static Date strToDate(String str, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
			return sdf.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
        return null;
    }
    
	/**
	 * yyyyMMddHHmmss
	 * @return
	 */
	public static String currentDateTime(){
		
		return format(new Date(),DATETIME_FORMAT );
	}
	
	
    /**
     * 当前月第一天
     * @return
     */
    public static String currMonthFirstDate(){
    	
    	SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT);

    	//获取当前月第一天：
    	Calendar c = Calendar.getInstance();    
    	c.add(Calendar.MONTH, 0);
    	//设置为1号,当前日期既为本月第一天 
    	c.set(Calendar.DAY_OF_MONTH,1);
    	String firstDate = fmt.format(c.getTime());
    	
    	return firstDate;
    }
    
    /**
     * 指定月最后1天
     * @param month yyyymm格式的字符串否则返回当前月最后1天
     * @return yyyyMMdd
     */
    public static String currMonthLastDate(String month){
    	String lastDate = "";
    	boolean isMonth = true;
    	if(month==null){
    		isMonth = false;
    	}else{
    		SimpleDateFormat fmt = new SimpleDateFormat(MONTH_FORMAT);
    		try {
    			fmt.parse(month);
    			isMonth = true;
			} catch (Exception e) {
				e.printStackTrace();
				isMonth = false;
			}
    	}
    	SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT);
    	Calendar c = Calendar.getInstance();
    	if(isMonth){
    		c.set(Calendar.YEAR, Integer.valueOf(month.substring(0, 4)));
    		c.set(Calendar.MONTH, Integer.valueOf(month.substring(4))-1);
    		c.add(Calendar.MONTH, 1);
    		c.set(Calendar.DAY_OF_MONTH,0);
    		lastDate = fmt.format(c.getTime());
    	}else{
    		c.add(Calendar.MONTH, 1);
    		c.set(Calendar.DAY_OF_MONTH,0);
    		lastDate = fmt.format(c.getTime());
    	}
    	return lastDate;
    }
    

    
    /**
     * 指定月最后1天(如果是当月，则返回当日)
     * @param month yyyymm格式的字符串否则返回当前月最后1天
     * @return yyyyMMdd
     */
    public static String currMonthLastDate2(String month){
    	String lastDate = "";
    	boolean isMonth = true;
    	if(month==null){
    		isMonth = false;
    	}else{
    		SimpleDateFormat fmt = new SimpleDateFormat(MONTH_FORMAT);
    		try {
    			fmt.parse(month);
    			isMonth = true;
    		} catch (Exception e) {
    			e.printStackTrace();
    			isMonth = false;
    		}
    	}
    	SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT);
    	Calendar c = Calendar.getInstance();
    	if(isMonth){
    		String currMonth = currentMonth();
    		if(currMonth.equals(month)){
    			lastDate = currentDay();
    		}else{
    			c.set(Calendar.YEAR, Integer.valueOf(month.substring(0, 4)));
    			c.set(Calendar.MONTH, Integer.valueOf(month.substring(4))-1);
    			c.add(Calendar.MONTH, 1);
    			c.set(Calendar.DAY_OF_MONTH,0);
    			lastDate = fmt.format(c.getTime());
    		}
    	}else{
    		c.add(Calendar.MONTH, 1);
    		c.set(Calendar.DAY_OF_MONTH,0);
    		lastDate = fmt.format(c.getTime());
    	}
    	return lastDate;
    }
    
    /**
     * 得到当月第1天0点
     * @return
     */
    public static Date getCurrMonthNumOneDay(){
    	SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT);
    	try {
			return fmt.parse(currMonthFirstDate());
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date();
		}
    }
	
    /**
     * 本月月第一天0点
     * @return
     */
    public static Date currMonthFirstDateTime(String date){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
    	
    	Calendar calendar = Calendar.getInstance(); 
    	
    	//获取当前时间：
    	try {
			calendar.setTime(sdf.parse(date));
			//设置为1号,日期既为本月第一天 
	    	calendar.set(Calendar.DAY_OF_MONTH,1);
	    	calendar.set(Calendar.HOUR_OF_DAY,0);
	    	calendar.set(Calendar.MINUTE,0);
	    	calendar.set(Calendar.SECOND,0);
	    	calendar.set(Calendar.MILLISECOND,0);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
    	return calendar.getTime();
    }
    
    /**
     * 获取指定时间的下月第一天0点
     * @return
     */
    public static Date nextMonthFirstDateTime(Date date){
    	
    	//获取当前时间：
    	Calendar c = Calendar.getInstance();
    	//设置到指定时间
    	if(date!=null){
    		c.setTime(date);
    	}
    	//月份推后1个月
    	c.add(Calendar.MONTH, 1);
    	//设置为1号,当前日期既为本月第一天 
    	c.set(Calendar.DAY_OF_MONTH,1);
    	c.set(Calendar.HOUR_OF_DAY,0);
    	c.set(Calendar.MINUTE,0);
    	c.set(Calendar.SECOND,0);
    	c.set(Calendar.MILLISECOND,0);
    	
    	return c.getTime();
    }
    
    /**
     * 将指定字符串格式的时间加上传入的天数，返回字符串格式的时间
     * @param dateStr yyyyMMdd
     * @param count 想要前进或倒退的天数
     * @return 到达指定的天数
     */
    public static String dateAdd(String dateStr, int count){
    	
    	SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
    	try {
			//20180510
    		//01234567
    		int year = Integer.valueOf(dateStr.substring(0,4));
    		int month = Integer.valueOf(dateStr.substring(4,6));
    		int date = Integer.valueOf(dateStr.substring(6));
    		
    		Calendar cal = Calendar.getInstance();
    		cal.set(Calendar.YEAR,year);//设置年份  
    		cal.set(Calendar.MONTH, month-1);//设置月份
    		cal.set(Calendar.DATE, date+count);//设置日期
    		
    		return sdf.format(cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
    
	
    public static void main(String[] args) {
    	
    	System.out.println(DateUtils.currentMonth());
	}
}