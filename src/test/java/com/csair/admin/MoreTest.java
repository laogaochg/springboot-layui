package com.csair.admin;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.alibaba.fastjson.JSON;
import com.csair.admin.config.UserIdConverter;
import com.csair.admin.util.XlsFileUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/7/24 17:30
 */
public class MoreTest {


    @Test
    public void testa() throws Exception {
        BufferedReader r = new BufferedReader(new FileReader("d:/a.txt"));
        BigDecimal a = new BigDecimal("0");
        while (r.ready()) {
            String s = r.readLine().split("\\|")[2];
            try {
                a = a.add(new BigDecimal(s));
            } catch (Exception e) {

            }
        }
        System.out.println(a);
        System.out.println(17962.178 - 175819.134);
    }

    @Test
    public void test() throws IntrospectionException {
        int size = 4569;
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(i);
        }
        //每条线程查询1000个号码
        for (int i = 0; i <= size / 1000; i++) {
            int end = size >= (i + 1) * 1000 ? (i + 1) * 1000  : size;
            System.out.println(i * 1000 + "-" + end);
            System.out.println(list.subList(i*1000,end));
        }
    }

    @Test
    public void testT() throws IntrospectionException {
        for (PropertyDescriptor pd : Introspector.getBeanInfo(AstTask.class, Object.class).getPropertyDescriptors()) {
            String name = pd.getName();
            String s = name.substring(0, 1).toUpperCase() + name.substring(1);
//            System.out.println(String.format("this.%s=d.get%s();", name,s));
            System.out.println(String.format("d.set%s(this.%s);", s, name));
        }
    }

    public static Long getTaskId() {
        DecimalFormat df = new DecimalFormat("00000");
        int anInt = new Random().nextInt(100000);
        return Long.valueOf(new Date().getTime() + "" + df.format(anInt));
    }
}
