package com.csair.admin;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/8/14 14:03
 */
public class TestEntity {
    /**
     * 语音套餐订购实例ID1
     */
    private String orderId;
    /**
     * 套餐产品ID1
     */
    private String pkgId;
    /**
     * 套内使用量（秒）
     */
    private String used;
    /**
     * 套内剩余量（秒)
     */
    private String residue;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPkgId() {
        return pkgId;
    }

    public void setPkgId(String pkgId) {
        this.pkgId = pkgId;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getResidue() {
        return residue;
    }

    public void setResidue(String residue) {
        this.residue = residue;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "orderId='" + orderId + '\'' +
                ", pkgId='" + pkgId + '\'' +
                ", used='" + used + '\'' +
                ", residue='" + residue + '\'' +
                '}';
    }
}
