package com.csair.admin;

import java.io.Serializable;
import java.util.Date;

public class DevicePortPo implements Serializable {
    private Long portid;

    private String portcode;

    private String portname;

    private Short typecode;

    private String originalrate;

    private String configrate;

    private String servicerate;

    private Short netmodel;

    private Short checkcode;

    private Short opencode;

    private Short usecode;

    private Short statuscode;

    private String relayport;

    private String bandmac;

    private String bandip;

    private Long unitid;

    private String centerid;

    private String roomid;

    private String frameid;

    private Long deviceid;

    private String modulcode;

    private Long customerid;

    private String remark;

    private Long operatorid;

    private Date inserttime;

    private Date updatetime;

    private Short datacode;

    private Date deletetime;
    /**
     * 端口索引号
     * 对应于之前项目json文件里面的ifIndex
     */
    private String interFace;

    private Short porttype;

    private String formid;

    private static final long serialVersionUID = 1L;

    public Long getPortid() {
        return portid;
    }

    public void setPortid(Long portid) {
        this.portid = portid;
    }

    public String getPortcode() {
        return portcode;
    }

    public void setPortcode(String portcode) {
        this.portcode = portcode == null ? null : portcode.trim();
    }

    public String getPortname() {
        return portname;
    }

    public void setPortname(String portname) {
        this.portname = portname == null ? null : portname.trim();
    }

    public Short getTypecode() {
        return typecode;
    }

    public void setTypecode(Short typecode) {
        this.typecode = typecode;
    }

    public String getOriginalrate() {
        return originalrate;
    }

    public void setOriginalrate(String originalrate) {
        this.originalrate = originalrate == null ? null : originalrate.trim();
    }

    public String getConfigrate() {
        return configrate;
    }

    public void setConfigrate(String configrate) {
        this.configrate = configrate == null ? null : configrate.trim();
    }

    public String getServicerate() {
        return servicerate;
    }

    public void setServicerate(String servicerate) {
        this.servicerate = servicerate == null ? null : servicerate.trim();
    }

    public Short getNetmodel() {
        return netmodel;
    }

    public void setNetmodel(Short netmodel) {
        this.netmodel = netmodel;
    }

    public Short getCheckcode() {
        return checkcode;
    }

    public void setCheckcode(Short checkcode) {
        this.checkcode = checkcode;
    }

    public Short getOpencode() {
        return opencode;
    }

    public void setOpencode(Short opencode) {
        this.opencode = opencode;
    }

    public Short getUsecode() {
        return usecode;
    }

    public void setUsecode(Short usecode) {
        this.usecode = usecode;
    }

    public Short getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(Short statuscode) {
        this.statuscode = statuscode;
    }

    public String getRelayport() {
        return relayport;
    }

    public void setRelayport(String relayport) {
        this.relayport = relayport == null ? null : relayport.trim();
    }

    public String getBandmac() {
        return bandmac;
    }

    public void setBandmac(String bandmac) {
        this.bandmac = bandmac == null ? null : bandmac.trim();
    }

    public String getBandip() {
        return bandip;
    }

    public void setBandip(String bandip) {
        this.bandip = bandip == null ? null : bandip.trim();
    }

    public Long getUnitid() {
        return unitid;
    }

    public void setUnitid(Long unitid) {
        this.unitid = unitid;
    }

    public String getCenterid() {
        return centerid;
    }

    public void setCenterid(String centerid) {
        this.centerid = centerid == null ? null : centerid.trim();
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }

    public String getFrameid() {
        return frameid;
    }

    public void setFrameid(String frameid) {
        this.frameid = frameid == null ? null : frameid.trim();
    }

    public Long getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(Long deviceid) {
        this.deviceid = deviceid;
    }

    public String getModulcode() {
        return modulcode;
    }

    public void setModulcode(String modulcode) {
        this.modulcode = modulcode == null ? null : modulcode.trim();
    }

    public Long getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Long customerid) {
        this.customerid = customerid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getOperatorid() {
        return operatorid;
    }

    public void setOperatorid(Long operatorid) {
        this.operatorid = operatorid;
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Short getDatacode() {
        return datacode;
    }

    public void setDatacode(Short datacode) {
        this.datacode = datacode;
    }

    public Date getDeletetime() {
        return deletetime;
    }

    public void setDeletetime(Date deletetime) {
        this.deletetime = deletetime;
    }


    public void setInterFace(String interFace) {
        this.interFace = interFace == null ? null : interFace.trim();
    }

    public Short getPorttype() {
        return porttype;
    }

    public void setPorttype(Short porttype) {
        this.porttype = porttype;
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid == null ? null : formid.trim();
    }

    public String getInterFace() {
        return interFace;
    }

    @Override
    public String toString() {
        return "DevicePortPo{" +
                "portid=" + portid +
                ", portcode='" + portcode + '\'' +
                ", portname='" + portname + '\'' +
                ", typecode=" + typecode +
                ", originalrate='" + originalrate + '\'' +
                ", configrate='" + configrate + '\'' +
                ", servicerate='" + servicerate + '\'' +
                ", netmodel=" + netmodel +
                ", checkcode=" + checkcode +
                ", opencode=" + opencode +
                ", usecode=" + usecode +
                ", statuscode=" + statuscode +
                ", relayport='" + relayport + '\'' +
                ", bandmac='" + bandmac + '\'' +
                ", bandip='" + bandip + '\'' +
                ", unitid=" + unitid +
                ", centerid='" + centerid + '\'' +
                ", roomid='" + roomid + '\'' +
                ", frameid='" + frameid + '\'' +
                ", deviceid=" + deviceid +
                ", modulcode='" + modulcode + '\'' +
                ", customerid=" + customerid +
                ", remark='" + remark + '\'' +
                ", operatorid=" + operatorid +
                ", inserttime=" + inserttime +
                ", updatetime=" + updatetime +
                ", datacode=" + datacode +
                ", deletetime=" + deletetime +
                ", interFace='" + interFace + '\'' +
                ", porttype=" + porttype +
                ", formid='" + formid + '\'' +
                '}';
    }
}