package com.csair.admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LaoGaoChuang
 * @Date : 2018/9/7 14:26
 */
public class MyFileUtils {
    public static List<String> parseFile(String file) throws Exception {
        BufferedReader b = new BufferedReader(new FileReader(new File(file)));
        List<String> result = new ArrayList<>();
        while (b.ready()) {
            result.add(b.readLine().trim());
        }
        return result;
    }
}
